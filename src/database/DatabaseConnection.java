package database;

import constants.ApplicationConstants;
import exception.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
    private static Logger logger = LoggerFactory.getLogger(DatabaseConnection.class);

    public static Connection getConnection() throws BusinessException {
        Connection connection = null;
        try {
            Class.forName(ApplicationConstants.DRIVER_NAME).newInstance();
            connection = DriverManager.getConnection(ApplicationConstants.DB_URL, ApplicationConstants.DB_USERNAME,
                    ApplicationConstants.DB_PASSWORD);
        } catch (SQLException | ClassNotFoundException se) {
            throw new BusinessException(se);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new BusinessException(e);
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        logger.info("Connection Successful");
        return connection;
    }
}