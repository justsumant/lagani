package application.controller;

import com.sun.javafx.scene.control.skin.VirtualFlow;
import constants.ApplicationConstants;
import exception.BusinessException;
import implementation.BusinessImplementation;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.*;
import javafx.util.Callback;
import modal.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Set;

public class MainApplicationController {
    @FXML
    private TableView<DashboardItem> dashboardTableView;
    @FXML
    private TableColumn<DashboardItem, Integer> serialNoDashboard;
    @FXML
    private Pagination pagination;
    @FXML
    private Button btnLogout;
    @FXML
    private TextField searchCustomer;
    @FXML
    private ListView<Customers> customerListView;
    @FXML
    private Label nameViewLabel;
    @FXML
    private Label addressViewLabel;
    @FXML
    private Label fatherViewLabel;
    @FXML
    private Label contactViewLabel;
    @FXML
    private Label spouseViewLabel;
    @FXML
    private TableView<Items> itemsTableView;
    @FXML
    private TableColumn<Items, Integer> itemNo;
    @FXML
    private TableColumn<Items, String> viewInstallment;
    @FXML
    private TableColumn<Items, String> addInstallment;
    @FXML
    private TableColumn<Items, String> calculate;
    @FXML
    private Button btnAddNewItem;
    @FXML
    private BorderPane mainBorderPane;
    @FXML
    private TextField nameProfile;
    @FXML
    private TextField firmNameProfile;
    @FXML
    private TextField contactProfile;
    @FXML
    private TextField emailProfile;
    @FXML
    private TextField addressProfile;
    @FXML
    private TextField getCustomerID;
    @FXML
    private Button btnViewCustomer;
    @FXML
    private TextField getItemID;
    @FXML
    private Button btnViewItem;
    @FXML
    private TextField getInstallmentID;
    @FXML
    private Button btnViewInstallment;
    @FXML
    private Button btnViewBackup;
    @FXML
    private VBox customerAdminPanel;
    @FXML
    private TextField adminFullName;
    @FXML
    private TextField adminAddress;
    @FXML
    private TextField adminWard;
    @FXML
    private TextField adminFatherName;
    @FXML
    private TextField adminSpouseName;
    @FXML
    private TextField adminContactNumber;
    @FXML
    private TextField adminRemarks;
    @FXML
    private Button btnUpdateCustomer;
    @FXML
    private VBox itemAdminPanel;
    @FXML
    private TextField adminItemAmount;
    @FXML
    private ComboBox adminTypeChooser;
    @FXML
    private DatePicker adminStartDate;
    @FXML
    private TextField adminRate;
    @FXML
    private DatePicker adminDeadline;
    @FXML
    private TextArea adminDescription;
    @FXML
    private Button btnUpdateItem;
    @FXML
    private VBox installmentAdminPanel;
    @FXML
    private TextField adminInstallmentAmount;
    @FXML
    private TextField adminDepositor;
    @FXML
    private DatePicker adminDepositDate;
    @FXML
    private Button btnUpdateInstallment;
    @FXML
    private VBox backupAdminPanel;
    @FXML
    private Button btnSelectMySQL;
    @FXML
    private Label mySQLPathViewer;
    @FXML
    private Button btnBrowse;
    @FXML
    private Label adminPathViewer;
    @FXML
    private Button btnBackup;
    @FXML
    private Button btnRestoreBrowse;
    @FXML
    private Label adminRestorePathViewer;
    @FXML
    private Button btnRestore;

    private ObservableList<Customers> customersObservableList = FXCollections.observableArrayList();
    private BusinessImplementation businessImplementation = new BusinessImplementation();
    private Logger logger = LoggerFactory.getLogger(MainApplicationController.class);
    private Alert alertWarning = new Alert(Alert.AlertType.WARNING);
    private Alert alertSuccess = new Alert(AlertType.INFORMATION);
    private ObservableList<Items> itemsObservableList;
    private Customers selectionCustomer = new Customers();
    private int id = LoginController.id();
    private String searchString = "";
    private Stage window;
    private Window dialogWindow;
    private int rowsPerPage = 50;

    public MainApplicationController() {
        itemsObservableList = FXCollections.observableArrayList();
    }

    public void initialize() {
        alertSuccess.setTitle(ApplicationConstants.SUCCESS_DIALOG);
        alertWarning.setTitle(ApplicationConstants.WARNING_DIALOG);
    }

    /**
     * Creates Pagination for TableView in Dashboard Tab
     */
    @FXML
    public void dashboardTab() {
        pagination.setPageFactory(this::createPage);
    }

    /**
     * Display items according to Pagination
     *
     * @param pageIndex
     * @return
     */
    private Node createPage(int pageIndex) {
        int count = 0;
        ArrayList<DashboardItem> dashboardItem = new ArrayList<>();
        try {
            count = businessImplementation.countDashboardItem();
            dashboardItem = businessImplementation.getDashboardItem(pageIndex * rowsPerPage);
        } catch (BusinessException e) {
            logger.error("" + e);
        }
        ObservableList<DashboardItem> dashboardItemObservableList = FXCollections.observableArrayList(dashboardItem);
        int itemsPerPage = 1;
        int page = pageIndex * itemsPerPage;
        int pageCount = (count / rowsPerPage) + 1;
        for (int iterator = page; iterator < page + itemsPerPage; iterator++) {
            TableView<DashboardItem> table = new TableView<>();
            serialNoDashboard.setCellValueFactory(cellData -> new ReadOnlyObjectWrapper<>(
                    dashboardTableView.getItems().indexOf(cellData.getValue()) + (pageIndex * rowsPerPage) + 1));
            dashboardTableView.setItems(dashboardItemObservableList);
            table.setItems(FXCollections.observableArrayList(dashboardItemObservableList.subList(0, dashboardItemObservableList.size())));
        }
        pagination.setPageCount(pageCount);
        return new BorderPane(dashboardTableView);
    }

    /**
     * Searching of Customers
     * Gets selected Customer for showing items
     */
    @FXML
    public void viewCustomers() {
        getCustomersForView(searchString);
        searchCustomer.textProperty().addListener(((observable, oldValue, newValue) -> {
            searchString = newValue;
            getCustomersForView(searchString);
        }));
        customerListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        btnAddNewItem.setDisable(true);

        customerListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (null != newValue) {
                btnAddNewItem.setDisable(false);
                Customers selectedCustomer = customerListView.getSelectionModel().getSelectedItem();
                nameViewLabel.setText(selectedCustomer.getFullName());
                fatherViewLabel.setText("F: " + selectedCustomer.getFatherName());
                addressViewLabel.setText(selectedCustomer.getAddress());
                spouseViewLabel.setText("S: " + selectedCustomer.getSpouseName());
                contactViewLabel.setText("" + selectedCustomer.getContactNo());
                selectionCustomer = selectedCustomer;
                fetchCustomerItem(selectedCustomer.getCustomerID());
            }
        });
    }

    /**
     * Get Customers to be displayed in ListView
     *
     * @param searchString
     */
    private void getCustomersForView(String searchString) {
        ArrayList<Customers> customerList = null;
        try {
            customerList = businessImplementation.getCustomers(searchString);
        } catch (BusinessException e) {
            logger.error("" + e);
        }
        customersObservableList.removeAll(customersObservableList);
        for (Customers customers : customerList) {
            customersObservableList.add(customers);
        }
        customerListView.getItems().clear();
        customerListView.getItems().addAll(customersObservableList);
        customerListView.setCellFactory(new Callback<ListView<Customers>, ListCell<Customers>>() {
            @Override
            public ListCell<Customers> call(ListView<Customers> param) {
                ListCell<Customers> cell = new ListCell<Customers>() {
                    private Tooltip tooltip = new Tooltip();

                    @Override
                    protected void updateItem(Customers customers, boolean empty) {
                        super.updateItem(customers, empty);
                        int index = this.getIndex();
                        if (empty || customers == null) {
                            setText("");
                            setTooltip(null);
                        } else {
                            setText((index + 1) + ". " + customers.getFullName() + ", " + customers.getAddress());
                            tooltip.setText("" + customers.getCustomerID());
                            setTooltip(tooltip);
                        }
                    }
                };
                return cell;
            }
        });
    }

    /**
     * Fetches Items of Customer and displays in TableView
     *
     * @param id
     */
    private void fetchCustomerItem(int id) {
        ArrayList<Items> itemList = null;
        try {
            itemList = businessImplementation.getItems(id);
        } catch (BusinessException e) {
            logger.error("" + e);
        }
        itemsObservableList = FXCollections.observableArrayList(itemList);
        itemNo.setCellValueFactory(cellData -> new ReadOnlyObjectWrapper<>(itemsTableView.getItems().indexOf(cellData.getValue()) + 1));

        viewInstallment.setCellValueFactory(new PropertyValueFactory<>(""));
        Callback<TableColumn<Items, String>, TableCell<Items, String>> cellFactoryView = new Callback<TableColumn<Items, String>, TableCell<Items, String>>() {
            @Override
            public TableCell call(final TableColumn<Items, String> param) {
                final TableCell<Items, String> cell = new TableCell<Items, String>() {
                    final Button btnViewInstallment = new Button("View");

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText("");
                        } else {
                            btnViewInstallment.setOnAction(event -> {
                                Items selectionItem = getTableView().getItems().get(getIndex());
                                Dialog<ButtonType> dialog = new Dialog<>();
                                dialog.initOwner(mainBorderPane.getScene().getWindow());
                                dialog.setTitle("Installments");
                                FXMLLoader fxmlLoader = new FXMLLoader();
                                fxmlLoader.setLocation(getClass().getResource("../layout/viewInstallmentDialog.fxml"));
                                dialogWindow = dialog.getDialogPane().getScene().getWindow();
                                try {
                                    dialog.getDialogPane().setContent(fxmlLoader.load());
                                    ViewInstallmentDialogController viewInstallmentDialogController = fxmlLoader.getController();
                                    viewInstallmentDialogController.initialize(selectionItem);
                                    dialogWindow.setOnCloseRequest(closeEvent -> dialog.close());
                                    dialog.showAndWait();
                                } catch (IOException e) {
                                    logger.error("" + e);
                                }
                            });
                            setGraphic(btnViewInstallment);
                            setText("");
                        }
                    }
                };
                return cell;
            }
        };
        viewInstallment.setCellFactory(cellFactoryView);

        addInstallment.setCellValueFactory(new PropertyValueFactory<>(""));
        Callback<TableColumn<Items, String>, TableCell<Items, String>> cellFactoryAdd = new Callback<TableColumn<Items, String>, TableCell<Items, String>>() {
            @Override
            public TableCell call(final TableColumn<Items, String> param) {
                final TableCell<Items, String> cell = new TableCell<Items, String>() {
                    final Button btnAddInstallment = new Button("Add");

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText("");
                        } else {
                            Items selectedItem = getTableView().getItems().get(getIndex());
                            if (selectedItem.getIsActive() == 1) {
                                btnAddInstallment.setOnAction(event -> {
                                    Dialog<ButtonType> dialog = new Dialog<>();
                                    dialog.initOwner(mainBorderPane.getScene().getWindow());
                                    dialog.setTitle("Add Installment");
                                    FXMLLoader fxmlLoader = new FXMLLoader();
                                    fxmlLoader.setLocation(getClass().getResource("../layout/addInstallmentDialog.fxml"));
                                    dialogWindow = dialog.getDialogPane().getScene().getWindow();
                                    try {
                                        dialog.getDialogPane().setContent(fxmlLoader.load());
                                        AddInstallmentDialogController addInstallmentDialogController = fxmlLoader.getController();
                                        addInstallmentDialogController.initialize(selectedItem);
                                        dialogWindow.setOnCloseRequest(closeEvent -> dialog.close());
                                        dialog.showAndWait();
                                    } catch (IOException e) {
                                        logger.error("" + e);
                                    }
                                });
                            } else {
                                btnAddInstallment.setDisable(true);
                            }
                            setGraphic(btnAddInstallment);
                            setText(null);
                        }
                    }
                };
                return cell;
            }
        };
        addInstallment.setCellFactory(cellFactoryAdd);

        calculate.setCellValueFactory(new PropertyValueFactory<>(""));
        Callback<TableColumn<Items, String>, TableCell<Items, String>> cellFactoryCalculate = new Callback<TableColumn<Items, String>, TableCell<Items, String>>() {
            @Override
            public TableCell call(final TableColumn<Items, String> param) {
                final TableCell<Items, String> cell = new TableCell<Items, String>() {
                    final Button btnCalculate = new Button("Calculate");

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText("");
                        } else {
                            btnCalculate.setOnAction(event -> {
                                Items items = getTableView().getItems().get(getIndex());
                                window = new Stage();
                                window.initOwner(mainBorderPane.getScene().getWindow());
                                window.setTitle("Calculation");
                                try {
                                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../layout/calculation.fxml"));
                                    Parent root = fxmlLoader.load();
                                    CalculationController calculationController = fxmlLoader.getController();
                                    calculationController.initialize(items);
                                    Scene scene = new Scene(root, 800, 500);
                                    window.setScene(scene);
                                    window.showAndWait();
                                } catch (IOException e) {
                                    logger.error("" + e);
                                }
                                fetchCustomerItem(id);
                            });
                            setGraphic(btnCalculate);
                            setText("");
                        }
                    }
                };
                return cell;
            }
        };
        calculate.setCellFactory(cellFactoryCalculate);

        itemsTableView.setItems(itemsObservableList);
        ScrollBar table1HorizontalScrollBar = findScrollBar(itemsTableView, Orientation.HORIZONTAL);
        ScrollBar table1VerticalScrollBar = findScrollBar(itemsTableView, Orientation.VERTICAL);
        assert table1HorizontalScrollBar != null;
        table1HorizontalScrollBar.setVisible(true);
        assert table1VerticalScrollBar != null;
        table1VerticalScrollBar.setVisible(false);
        VirtualFlow flow = (VirtualFlow) itemsTableView.lookup(".virtual-flow");
        flow.requestLayout();
    }

    /**
     * Property for Scrollbar
     *
     * @param table
     * @param orientation
     * @return
     */
    private ScrollBar findScrollBar(TableView<?> table, Orientation orientation) {
        Set<Node> set = table.lookupAll(".scroll-bar");
        for (Node node : set) {
            ScrollBar bar = (ScrollBar) node;
            if (bar.getOrientation() == orientation) {
                return bar;
            }
        }
        return null;
    }

    /**
     * Adds new Customer
     */
    @FXML
    public void addNewCustomer() {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.initOwner(mainBorderPane.getScene().getWindow());
        dialog.setTitle("Add New Customer");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("../layout/addNewCustomerDialog.fxml"));
        dialogWindow = dialog.getDialogPane().getScene().getWindow();
        try {
            dialog.getDialogPane().setContent(fxmlLoader.load());
            AddNewCustomerDialogController addNewCustomerDialogController = fxmlLoader.getController();
            addNewCustomerDialogController.initialize();
            dialogWindow.setOnCloseRequest(event -> dialog.close());
            dialog.showAndWait();
        } catch (IOException e) {
            logger.error("" + e);
        }
        viewCustomers();
    }

    /**
     * Adds new Item for selected Customer
     */
    @FXML
    public void addNewItem() {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.initOwner(mainBorderPane.getScene().getWindow());
        dialog.setTitle("Add New Item");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("../layout/addNewItemDialog.fxml"));
        dialogWindow = dialog.getDialogPane().getScene().getWindow();
        try {
            dialog.getDialogPane().setContent(fxmlLoader.load());
            AddNewItemDialogController addNewItemDialogController = fxmlLoader.getController();
            addNewItemDialogController.updateDialogBox(selectionCustomer);
            dialogWindow.setOnCloseRequest((WindowEvent event) -> dialog.close());
            dialog.onCloseRequestProperty();
            fetchCustomerItem(selectionCustomer.getCustomerID());
            dialog.showAndWait();
        } catch (IOException e) {
            logger.error("" + e);
        }
        fetchCustomerItem(selectionCustomer.getCustomerID());
    }

    /**
     * View User data in Profile Tab
     */
    @FXML
    public void profileTab() {
        User user = null;
        try {
            user = businessImplementation.getUser(id);
        } catch (BusinessException e) {
            logger.error("" + e);
        }
        nameProfile.setText(user.getName());
        firmNameProfile.setText(user.getFirmname());
        contactProfile.setText("" + user.getContact());
        emailProfile.setText(user.getEmail());
        addressProfile.setText(user.getAddress());
    }

    /**
     * Updates User Profile
     */
    @FXML
    public void updateProfile() {
        User user = new User();
        if (nameProfile.getText().length() <= 0) {
            nameProfile.setStyle(ApplicationConstants.ERROR_ENTRY);
            alertWarning.setContentText("Enter name");
            alertWarning.showAndWait();
        } else if (firmNameProfile.getText().length() <= 0) {
            firmNameProfile.setStyle(ApplicationConstants.ERROR_ENTRY);
            alertWarning.setContentText("Enter firm name");
            alertWarning.showAndWait();
        } else if (!contactProfile.getText().matches(ApplicationConstants.CONTACT_NUMBER_VALIDATION_REGEX)) {
            contactProfile.setStyle(ApplicationConstants.ERROR_ENTRY);
            alertWarning.setContentText("Enter valid contact number");
            alertWarning.showAndWait();
        } else if (!emailProfile.getText().matches(ApplicationConstants.EMAIL_VALIDATION_REGEX)) {
            emailProfile.setStyle(ApplicationConstants.ERROR_ENTRY);
            alertWarning.setContentText("Enter valid email address");
            alertWarning.showAndWait();
        } else if (addressProfile.getText().length() <= 0) {
            addressProfile.setStyle(ApplicationConstants.ERROR_ENTRY);
            alertWarning.setContentText("Enter address");
            alertWarning.showAndWait();
        } else {
            nameProfile.setStyle(ApplicationConstants.CORRECT_ENTRY);
            firmNameProfile.setStyle(ApplicationConstants.CORRECT_ENTRY);
            contactProfile.setStyle(ApplicationConstants.CORRECT_ENTRY);
            addressProfile.setStyle(ApplicationConstants.CORRECT_ENTRY);

            user.setName(nameProfile.getText());
            user.setFirmname(firmNameProfile.getText());
            user.setContact(Long.parseLong(contactProfile.getText()));
            user.setEmail(emailProfile.getText());
            user.setAddress(addressProfile.getText());

            try {
                businessImplementation.updateUserProfile(user, id);
            } catch (BusinessException e) {
                logger.error("" + e);
            }
            alertSuccess.setContentText("Profile updated successfully");
            alertSuccess.showAndWait();
        }
    }

    /**
     * Accepts Id of data that needs to be modified
     * Backup button included
     */
    @FXML
    public void adminTab() {
        customerAdminPanel.setVisible(false);
        itemAdminPanel.setVisible(false);
        installmentAdminPanel.setVisible(false);
        backupAdminPanel.setVisible(false);
        btnViewCustomer.setOnAction(event -> {
            String customerID = getCustomerID.getText();
            if (!customerID.matches(ApplicationConstants.WARD_ID_VALIDATION_REGEX)) {
                getCustomerID.setStyle(ApplicationConstants.ERROR_ENTRY);
                alertWarning.setContentText("Enter valid customer ID");
                alertWarning.showAndWait();
            } else {
                try {
                    Customers customers = businessImplementation.getCustomerByID(Integer.parseInt(customerID));
                    if (customers == null) {
                        alertWarning.setContentText("Enter valid customer ID");
                        alertWarning.showAndWait();
                    } else {
                        getCustomerID.setStyle(ApplicationConstants.CORRECT_ENTRY);
                        adminViewCustomer(customers);
                    }
                } catch (BusinessException e) {
                    logger.error("" + e);
                }
            }
        });
        btnViewItem.setOnAction(event -> {
            String itemID = getItemID.getText();
            if (!itemID.matches(ApplicationConstants.WARD_ID_VALIDATION_REGEX)) {
                getItemID.setStyle(ApplicationConstants.ERROR_ENTRY);
                alertWarning.setContentText("Enter valid item ID");
                alertWarning.showAndWait();
            } else {
                try {
                    Items items = businessImplementation.getItemByID(Integer.parseInt(itemID));
                    if (null == items) {
                        alertWarning.setContentText("Enter valid item ID");
                        alertWarning.showAndWait();
                    } else {
                        getItemID.setStyle(ApplicationConstants.CORRECT_ENTRY);
                        adminViewItem(items);
                    }
                } catch (BusinessException e) {
                    logger.error("" + e);
                }
            }
        });
        btnViewInstallment.setOnAction(event -> {
            String installmentID = getInstallmentID.getText();
            if (!installmentID.matches(ApplicationConstants.WARD_ID_VALIDATION_REGEX)) {
                getInstallmentID.setStyle(ApplicationConstants.ERROR_ENTRY);
                alertWarning.setContentText("Enter valid installment ID");
                alertWarning.showAndWait();
            } else {
                try {
                    Installment installment = businessImplementation.getInstallmentByID(Integer.parseInt(installmentID));
                    if (null == installment) {
                        alertWarning.setContentText("Enter valid installment ID");
                        alertWarning.showAndWait();
                    } else {
                        getInstallmentID.setStyle(ApplicationConstants.CORRECT_ENTRY);
                        adminViewInstallment(installment);
                    }
                } catch (BusinessException e) {
                    logger.error("" + e);
                }
            }
        });
        btnViewBackup.setOnAction(event -> {
            backup();
        });
    }

    /**
     * View Customer data that needs to be updated
     *
     * @param customers
     */
    private void adminViewCustomer(Customers customers) {
        customerAdminPanel.setVisible(true);
        itemAdminPanel.setVisible(false);
        installmentAdminPanel.setVisible(false);
        backupAdminPanel.setVisible(false);

        adminFullName.setText(customers.getFullName());
        adminAddress.setText(customers.getAddress());
        adminWard.setText("" + customers.getWard());
        adminFatherName.setText(customers.getFatherName());
        adminSpouseName.setText(customers.getSpouseName());
        adminContactNumber.setText("" + customers.getContactNo());
        adminRemarks.setText(customers.getRemarks());
        btnUpdateCustomer.setOnAction(event -> {
            if (adminFullName.getText().length() <= 0) {
                adminFullName.setStyle(ApplicationConstants.ERROR_ENTRY);
                alertWarning.setContentText("Enter full name");
                alertWarning.showAndWait();
            } else if (adminAddress.getText().length() <= 0) {
                adminAddress.setStyle(ApplicationConstants.ERROR_ENTRY);
                alertWarning.setContentText("Enter address");
                alertWarning.showAndWait();
            } else if (!adminWard.getText().matches(ApplicationConstants.WARD_ID_VALIDATION_REGEX)) {
                adminWard.setStyle(ApplicationConstants.ERROR_ENTRY);
                alertWarning.setContentText("Enter valid ward number");
                alertWarning.showAndWait();
            } else if (adminFatherName.getText().length() <= 0) {
                adminFatherName.setStyle(ApplicationConstants.ERROR_ENTRY);
                alertWarning.setContentText("Enter father's name");
                alertWarning.showAndWait();
            } else if (adminSpouseName.getText().length() <= 0) {
                adminSpouseName.setStyle(ApplicationConstants.ERROR_ENTRY);
                alertWarning.setContentText("Enter spouse's name");
                alertWarning.showAndWait();
            } else if (!adminContactNumber.getText().matches(ApplicationConstants.CONTACT_NUMBER_VALIDATION_REGEX)) {
                adminContactNumber.setStyle(ApplicationConstants.ERROR_ENTRY);
                alertWarning.setContentText("Enter valid contact number");
                alertWarning.showAndWait();
            } else if (adminRemarks.getText().length() <= 0) {
                adminRemarks.setStyle(ApplicationConstants.ERROR_ENTRY);
                alertWarning.setContentText("Enter remarks");
                alertWarning.showAndWait();
            } else {
                adminFullName.setStyle(ApplicationConstants.CORRECT_ENTRY);
                adminAddress.setStyle(ApplicationConstants.CORRECT_ENTRY);
                adminWard.setStyle(ApplicationConstants.CORRECT_ENTRY);
                adminFatherName.setStyle(ApplicationConstants.CORRECT_ENTRY);
                adminSpouseName.setStyle(ApplicationConstants.CORRECT_ENTRY);
                adminContactNumber.setStyle(ApplicationConstants.CORRECT_ENTRY);
                adminRemarks.setStyle(ApplicationConstants.CORRECT_ENTRY);

                customers.setFullName(adminFullName.getText());
                customers.setAddress(adminAddress.getText());
                customers.setWard(Integer.parseInt(adminWard.getText()));
                customers.setFatherName(adminFatherName.getText());
                customers.setSpouseName(adminSpouseName.getText());
                customers.setContactNo(Long.parseLong(adminContactNumber.getText()));
                customers.setRemarks(adminRemarks.getText());
                try {
                    businessImplementation.updateCustomerData(customers);
                } catch (BusinessException e) {
                    logger.error("" + e);
                }
                alertSuccess.setContentText("Customer updated successfully");
                alertSuccess.showAndWait();
            }
        });
    }

    /**
     * View Item data that needs to be updated
     *
     * @param items
     */
    private void adminViewItem(Items items) {
        customerAdminPanel.setVisible(false);
        itemAdminPanel.setVisible(true);
        installmentAdminPanel.setVisible(false);
        backupAdminPanel.setVisible(false);

        adminItemAmount.setText(items.getPrincipal());
        adminTypeChooser.setValue(items.getType());
        adminStartDate.setValue(items.getStartDate().toLocalDate());
        adminRate.setText(items.getRate());
        adminDeadline.setValue(items.getDeadline().toLocalDate());
        adminDescription.setText(items.getDescription());
        btnUpdateItem.setOnAction(event -> {
            if (!adminItemAmount.getText().matches(ApplicationConstants.NUMBER_VALIDATION_REGEX)) {
                adminItemAmount.setStyle(ApplicationConstants.ERROR_ENTRY);
                alertWarning.setContentText("Input fields not valid");
                alertWarning.showAndWait();
            } else if (!(adminStartDate.getValue().compareTo(LocalDate.now()) <= 0)) {
                adminStartDate.setStyle(ApplicationConstants.ERROR_ENTRY);
                alertWarning.setContentText("Date field should not be in future");
                alertWarning.showAndWait();
            } else if (!adminRate.getText().matches(ApplicationConstants.NUMBER_VALIDATION_REGEX)) {
                adminRate.setStyle(ApplicationConstants.ERROR_ENTRY);
                alertWarning.setContentText("Rate should be in decimal");
                alertWarning.showAndWait();
            } else if (!(adminDeadline.getValue().compareTo(LocalDate.now()) > 0)) {
                adminDeadline.setStyle(ApplicationConstants.ERROR_ENTRY);
                alertWarning.setContentText("Deadline should be in future");
                alertWarning.showAndWait();
            } else if (adminDescription.getText().length() <= 0) {
                adminDescription.setStyle(ApplicationConstants.ERROR_ENTRY);
                alertWarning.setContentText("Enter description");
                alertWarning.showAndWait();
            } else {
                adminItemAmount.setStyle(ApplicationConstants.CORRECT_ENTRY);
                adminStartDate.setStyle(ApplicationConstants.CORRECT_ENTRY);
                adminRate.setStyle(ApplicationConstants.CORRECT_ENTRY);
                adminDeadline.setStyle(ApplicationConstants.CORRECT_ENTRY);
                adminDescription.setStyle(ApplicationConstants.CORRECT_ENTRY);

                items.setPrincipal(adminItemAmount.getText());
                items.setStartDate(Date.valueOf(adminStartDate.getValue()));
                items.setRate(adminRate.getText());
                items.setDeadline(Date.valueOf(adminDeadline.getValue()));
                items.setDescription(adminDescription.getText());
                try {
                    businessImplementation.updateItemData(items);
                } catch (BusinessException e) {
                    logger.error("" + e);
                }
                alertSuccess.setContentText("Item updated successfully");
                alertSuccess.showAndWait();
            }
        });
    }

    /**
     * View Installment data that needs to be updated
     *
     * @param installment
     */
    private void adminViewInstallment(Installment installment) {
        customerAdminPanel.setVisible(false);
        itemAdminPanel.setVisible(false);
        installmentAdminPanel.setVisible(true);
        backupAdminPanel.setVisible(false);

        adminInstallmentAmount.setText("" + installment.getDepositAmount());
        adminDepositor.setText(installment.getDepositor());
        adminDepositDate.setValue(installment.getDate().toLocalDate());
        btnUpdateInstallment.setOnAction(event -> {
            if (!adminInstallmentAmount.getText().matches(ApplicationConstants.NUMBER_VALIDATION_REGEX)) {
                adminInstallmentAmount.setStyle(ApplicationConstants.ERROR_ENTRY);
                alertWarning.setContentText("Input fields not valid");
                alertWarning.showAndWait();
            } else if (adminDepositor.getText().length() <= 0) {
                adminDepositor.setStyle(ApplicationConstants.ERROR_ENTRY);
                alertWarning.setContentText("Enter Depositor Name");
                alertWarning.showAndWait();
            } else if (!(adminDepositDate.getValue().compareTo(LocalDate.now()) <= 0)) {
                adminDepositDate.setStyle(ApplicationConstants.ERROR_ENTRY);
                alertWarning.setContentText("Date field should not be in future");
                alertWarning.showAndWait();
            } else {
                adminInstallmentAmount.setStyle(ApplicationConstants.CORRECT_ENTRY);
                adminDepositor.setStyle(ApplicationConstants.CORRECT_ENTRY);
                adminDepositDate.setStyle(ApplicationConstants.CORRECT_ENTRY);

                installment.setDepositAmount(Integer.parseInt(adminInstallmentAmount.getText()));
                installment.setDepositor(adminDepositor.getText());
                installment.setDate(java.sql.Date.valueOf(adminDepositDate.getValue()));
                try {
                    businessImplementation.updateInstallmentData(installment);
                } catch (BusinessException e) {
                    logger.error("" + e);
                }
                alertSuccess.setContentText("Installment data updated successfully");
                alertSuccess.showAndWait();
            }
        });
    }

    /**
     * Backup User Data
     */
    private void backup() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ApplicationConstants.BACKUP_DATE_FORMAT);
        String mySQLDumpPath = null;
        try {
            mySQLDumpPath = businessImplementation.getMySQLDumpPath();
        } catch (BusinessException e) {
            logger.error("" + e);
        }
        String formatDateTime = LocalDateTime.now().format(formatter);
        DirectoryChooser directoryChooser = new DirectoryChooser();
        mySQLPathViewer.setText(mySQLDumpPath);
        customerAdminPanel.setVisible(false);
        itemAdminPanel.setVisible(false);
        installmentAdminPanel.setVisible(false);
        backupAdminPanel.setVisible(true);
        btnBackup.setVisible(false);
        btnRestore.setVisible(false);
        btnBrowse.setOnAction(event -> {
            File selectedDirectory = directoryChooser.showDialog(mainBorderPane.getScene().getWindow());
            if (null != selectedDirectory) {
                File file = new File(selectedDirectory + "/Lagani-dump-" + formatDateTime + ".sql");
                adminPathViewer.setText(String.valueOf(file));
                btnBackup.setVisible(true);
                btnBackup.setOnAction(event1 -> {
                    String finalMySQLDumpPath;
                    try {
                        finalMySQLDumpPath = businessImplementation.getMySQLDumpPath();
                        file.createNewFile();
                        backupDatabase(finalMySQLDumpPath, file);
                    } catch (IOException | BusinessException e) {
                        logger.error("" + e);
                    }
                });
            }
        });
        btnSelectMySQL.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            File selectedFile = fileChooser.showOpenDialog(mainBorderPane.getScene().getWindow());
            if (null != selectedFile) {
                mySQLPathViewer.setText(String.valueOf(selectedFile));
                try {
                    businessImplementation.updateMySQLDumpPath(String.valueOf(selectedFile));
                } catch (BusinessException e) {
                    logger.error("" + e);
                }
            }
        });
        btnRestoreBrowse.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            File selectedFile = fileChooser.showOpenDialog(mainBorderPane.getScene().getWindow());
            if (null != selectedFile) {
                adminRestorePathViewer.setText(String.valueOf(selectedFile));
                btnRestore.setVisible(true);
                btnRestore.setOnAction(event1 -> restoreDatabase(selectedFile));
            }
        });
    }

    /**
     * Backup data
     *
     * @param path
     */
    private void backupDatabase(String mySQLDumpPath, File path) {
        String executeCmd = mySQLDumpPath + " -u" + ApplicationConstants.DB_USERNAME + " -p" + ApplicationConstants.DB_PASSWORD + " --add-drop-database -B " + ApplicationConstants.DB_NAME + " -r" + path;
        Process runtimeProcess;
        try {
            runtimeProcess = Runtime.getRuntime().exec(executeCmd);
            int processComplete = runtimeProcess.waitFor();
            if (processComplete == 0) {
                alertSuccess.setContentText("Backup created successfully at " + path);
                alertSuccess.showAndWait();
            } else {
                alertWarning.setContentText("Could not create the backup");
                alertWarning.showAndWait();
            }
        } catch (Exception e) {
            logger.error("" + e);
        }
    }

    /**
     * Restore database
     *
     * @param source
     */
    private void restoreDatabase(File source) {
        String[] restoreCmd = new String[]{"C:/Program Files/MySQL/MySQL Server 8.0/bin/mysql.exe ", "--user=" + ApplicationConstants.DB_USERNAME, "--password=" + ApplicationConstants.DB_PASSWORD, "-e", "source " + source};
        Process runtimeProcess;
        try {
            runtimeProcess = Runtime.getRuntime().exec(restoreCmd);
            int processComplete = runtimeProcess.waitFor();
            if (processComplete == 0) {
                alertSuccess.setContentText("Backup restored successfully from " + source);
                alertSuccess.showAndWait();
            } else {
                alertWarning.setContentText("Could not restore the backup");
                alertWarning.showAndWait();
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Logout code
     */
    @FXML
    public void logout() {
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("../layout/loginPage.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Lagani");
            stage.setScene(new Scene(root, 720, 480));
            stage.show();
            Scene scene = btnLogout.getScene();
            if (scene != null) {
                Window window = scene.getWindow();
                if (window != null) {
                    window.hide();
                }
            }
        } catch (IOException e) {
            logger.error("" + e);
        }
    }
}
