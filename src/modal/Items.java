package modal;

import javafx.beans.property.SimpleStringProperty;

import java.sql.Date;

public class Items {
    private SimpleStringProperty itemID = new SimpleStringProperty("");
    private SimpleStringProperty type = new SimpleStringProperty("");
    private Date startDate;
    private SimpleStringProperty principal = new SimpleStringProperty("");
    private SimpleStringProperty rate = new SimpleStringProperty("");
    private SimpleStringProperty description = new SimpleStringProperty("");
    private SimpleStringProperty status = new SimpleStringProperty("");
    private SimpleStringProperty createdAt = new SimpleStringProperty("");
    private SimpleStringProperty updatedAt = new SimpleStringProperty("");
    private SimpleStringProperty closerName = new SimpleStringProperty("");
    private SimpleStringProperty totalAmount = new SimpleStringProperty("");
    private SimpleStringProperty closingAmount = new SimpleStringProperty("");
    private int isActive;
    private Date deadline;
    private SimpleStringProperty closingDate = new SimpleStringProperty("");

    public String getItemID() {
        return itemID.get();
    }

    public SimpleStringProperty itemIDProperty() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID.set(itemID);
    }

    public String getType() {
        return type.get();
    }

    public SimpleStringProperty typeProperty() {
        return type;
    }

    public void setType(String type) {
        this.type.set(type);
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getPrincipal() {
        return principal.get();
    }

    public SimpleStringProperty principalProperty() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal.set(principal);
    }

    public String getRate() {
        return rate.get();
    }

    public SimpleStringProperty rateProperty() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate.set(rate);
    }

    public String getDescription() {
        return description.get();
    }

    public SimpleStringProperty descriptionProperty() {
        return description;
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public String getStatus() {
        return status.get();
    }

    public SimpleStringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    public String getCreatedAt() {
        return createdAt.get();
    }

    public SimpleStringProperty createdAtProperty() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt.set(createdAt);
    }

    public String getUpdatedAt() {
        return updatedAt.get();
    }

    public SimpleStringProperty updatedAtProperty() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt.set(updatedAt);
    }

    public String getCloserName() {
        return closerName.get();
    }

    public SimpleStringProperty closerNameProperty() {
        return closerName;
    }

    public void setCloserName(String closerName) {
        this.closerName.set(closerName);
    }

    public String getTotalAmount() {
        return totalAmount.get();
    }

    public SimpleStringProperty totalAmountProperty() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount.set(totalAmount);
    }

    public String getClosingAmount() {
        return closingAmount.get();
    }

    public SimpleStringProperty closingAmountProperty() {
        return closingAmount;
    }

    public void setClosingAmount(String closingAmount) {
        this.closingAmount.set(closingAmount);
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getClosingDate() {
        return closingDate.get();
    }

    public SimpleStringProperty closingDateProperty() {
        return closingDate;
    }

    public void setClosingDate(String closingDate) {
        this.closingDate.set(closingDate);
    }

    @Override
    public String toString() {
        return "Items{" +
                "itemID=" + itemID +
                ", type=" + type +
                ", startDate=" + startDate +
                ", principal=" + principal +
                ", rate=" + rate +
                ", description=" + description +
                ", status=" + status +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", closerName=" + closerName +
                ", totalAmount=" + totalAmount +
                ", closingAmount=" + closingAmount +
                ", isActive=" + isActive +
                ", deadline=" + deadline +
                ", closingDate=" + closingDate +
                '}';
    }
}
